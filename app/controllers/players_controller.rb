class PlayersController < ApplicationController
  before_action :set_player, only: [:show, :edit, :update, :destroy]
  # GET /players
  # GET /players.json
  def index
    @players = Player.all
  end
  
  def player_balance(id)
    #user list matches
    player_matches = Match.where(player_id: id)
    player_matches += Match.where(next_player_id: id)

    #user is the winner
    player_winner = Match.where({player_id: id, winner_player_id: id})
    player_winner += Match.where({next_player_id: id, winner_player_id: id})

    #user lost matches
    player_lost = Match.where(player_id: id).where.not(winner_player_id: id)
    player_lost += Match.where(next_player_id: id).where.not(winner_player_id: id)

    sum = 0

    for i in player_winner do
      sum += 10
    end

    for i in player_lost do
      sum += i.result.to_i
    end

    total_matches = (player_lost.size + player_winner.size) 
    if total_matches > 0
    player_average = (sum.to_f / total_matches).round(2)
    else
    player_average = 0.to_f.round(2)
    end

    return player_average, player_matches, player_winner, player_lost
  end

  # GET /players/1
  # GET /players/1.json
  def show
    player_balance_array = player_balance(params[:id])
    # player_balance_array[0] return player_average
    # player_balance_array[1] return player_matches
    # player_balance_array[2] return player_winner
    # player_balance_array[3] return player_lost

    @player_average = player_balance_array[0]

    @player_matches = player_balance_array[1]

    @player_winner = player_balance_array[2]

    @player_lost = player_balance_array[3]

  end

  def ranking
    ranking_array_tmp = Array.new
    all_players = Player.all

    for p in all_players do

      player_balance_array = player_balance(p.id)
      ranking = player_balance_array[0] * player_balance_array[2].size
      ranking_array_tmp.push(PlayerBalanceClass.new(p.id, p.last_name, player_balance_array[0], player_balance_array[1].size, player_balance_array[2].size, ranking))

    end

    @ranking_array = ranking_array_tmp.sort_by {|x| x.ranking_player}.reverse

  end

  # GET /players/new
  def new
    @player = Player.new
  end

  # GET /players/1/edit
  def edit
  end

  # POST /players
  # POST /players.json
  def create
    @player = Player.new(player_params)

    respond_to do |format|
      if @player.save
        format.html { redirect_to @player, notice: 'Player was successfully created.' }
        format.json { render :show, status: :created, location: @player }
      else
        format.html { render :new }
        format.json { render json: @player.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /players/1
  # PATCH/PUT /players/1.json
  def update
    respond_to do |format|
      if @player.update(player_params)
        format.html { redirect_to @player, notice: 'Player was successfully updated.' }
        format.json { render :show, status: :ok, location: @player }
      else
        format.html { render :edit }
        format.json { render json: @player.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /players/1
  # DELETE /players/1.json
  def destroy
    @player.destroy
    respond_to do |format|
      format.html { redirect_to players_url, notice: 'Player was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_player
    @player = Player.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def player_params
    params.require(:player).permit(:first_name, :last_name, :avatar)
  end

  class PlayerBalanceClass
    def initialize(id, name, average, matches, winner, ranking)
      @id_player = id
      @last_name = name
      @average = average
      @number_matches = matches
      @number_winner_player = winner
      @ranking_player = ranking
    end

    def id_player
      return @id_player
    end

    def last_name
      return @last_name
    end

    def average
      return @average
    end

    def number_matches
      return @number_matches
    end

    def number_winner_player
      return @number_winner_player
    end

    def ranking_player
      return @ranking_player
    end
  end
end
