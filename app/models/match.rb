class Match < ActiveRecord::Base
  belongs_to :player
  belongs_to :next_player, :class_name => 'Player', :foreign_key => 'next_player_id'
  belongs_to :winner_player, :class_name => 'Player', :foreign_key => 'winner_player_id'
end
