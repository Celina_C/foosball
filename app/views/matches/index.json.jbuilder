json.array!(@matches) do |match|
  json.extract! match, :id, :player_id, :next_player_id, :date, :result, :winner_player_id
  json.url match_url(match, format: :json)
end
