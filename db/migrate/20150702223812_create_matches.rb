class CreateMatches < ActiveRecord::Migration
  def change
    create_table :matches do |t|
      t.references :player, index: true
      t.references :next_player, index: true
      t.datetime :date
      t.string :result
      t.references :winner_player, index: true

      t.timestamps
    end
  end
end
