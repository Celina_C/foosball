# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150705144914) do

  create_table "matches", force: true do |t|
    t.integer  "player_id"
    t.integer  "next_player_id"
    t.datetime "date"
    t.string   "result"
    t.integer  "winner_player_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "matches", ["next_player_id"], name: "index_matches_on_next_player_id"
  add_index "matches", ["player_id"], name: "index_matches_on_player_id"
  add_index "matches", ["winner_player_id"], name: "index_matches_on_winner_player_id"

  create_table "players", force: true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "avatar"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
  end

end
